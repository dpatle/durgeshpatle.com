import { Durgeshpatle.ComPage } from './app.po';

describe('durgeshpatle.com App', () => {
  let page: Durgeshpatle.ComPage;

  beforeEach(() => {
    page = new Durgeshpatle.ComPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
