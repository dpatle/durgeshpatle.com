/**
 * Created by durgesh on 5/3/17.
 */
import {Injectable} from '@angular/core';

function getWindow(): any {
  return window;
}

@Injectable()
export class WindowRefService {
  getNativeWindow(): any {
    return getWindow();
  }
}
