/**
 * Created by durgesh on 19/2/17.
 */
import {Injectable, EventEmitter} from '@angular/core';

@Injectable()
export class ApplicationDataService {
    selectedProfile: string = "public";
    public profileChange: EventEmitter<string>;

    constructor() {
        this.profileChange = new EventEmitter();
    }

    getSelectedProfile(): string {
        return this.selectedProfile;
    }

    setSelectedProfile(selectedProfile: string): void {
        this.selectedProfile = selectedProfile;
        this.profileChange.next(this.selectedProfile);
    }
}
