/**
 * Created by durgesh on 18/2/17.
 */
import { Component } from '@angular/core';

@Component({
    selector : 'footer',
    templateUrl : 'footer.component.html',
    styleUrls : [ 'footer.component.css']
})

export class FooterComponent { }
