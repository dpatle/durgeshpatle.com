/**
 * Created by durgesh on 18/2/17.
 */
import {Component, OnInit, NgModule} from '@angular/core';
import {BrowserModule, DOCUMENT} from '@angular/platform-browser';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import 'rxjs/add/operator/share';

import {ApplicationDataService} from '../shared/application-data.service';
import {AuthenticationService} from '../shared/authentication.service';
import {WindowRefService} from '../shared/window-ref.service';

@Component({
    selector: 'header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.css'],
    providers: [ApplicationDataService, AuthenticationService, WindowRefService]
})

export class HeaderComponent implements OnInit {
    dropDownState: boolean;
    selectedProfile: string;
    subscription: any;

    constructor(private applicationDataService: ApplicationDataService,
                private authenticationService: AuthenticationService,
                private windowRefService: WindowRefService) {
    }

    selectProfile(profile: string): void {
        this.dropDownState = false;
        if (profile === 'durgesh' && !this.authenticationService.getLoginStatus()) {
            this.navigateToUrl('/login');
        } else {
            this.selectedProfile = profile;
            this.applicationDataService.setSelectedProfile(this.selectedProfile);
        }
    }

    navigateToUrl(url: string): void {
        this.windowRefService.getNativeWindow().location.href = url;
    }

    ngOnInit(): void {
        this.dropDownState = false;
        this.selectedProfile = this.applicationDataService.getSelectedProfile();
        this.applicationDataService.profileChange.subscribe(data => {
            this.selectedProfile = data;
        });
    }
}
