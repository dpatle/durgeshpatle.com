import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule}    from '@angular/http';
import {FormsModule}    from '@angular/forms';

import {MovingBackgroundDirective} from './moving-background/moving-background.directive';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {LoginComponent} from './login/login.component';
import {GeekyComponent} from './geeky/geeky.component';

const appRoutes: Routes = [{
  path: '',
  component: DashboardComponent
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: 'geeky',
  component: GeekyComponent
}
]

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule
  ],
  declarations: [
    MovingBackgroundDirective,
    AppComponent,
    DashboardComponent,
    LoginComponent,
    GeekyComponent,
    HeaderComponent,
    FooterComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
