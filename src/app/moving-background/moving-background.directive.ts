/**
 * Created by durgesh on 18/3/17.
 */
import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[moving-background]'
})

export class MovingBackgroundDirective {

  private backgroundPosition = '50% 50%';
  private el :any;
  private mouseEvent : any;
  private mouseEventCount : number;
  private moveBackground(position : string) {
    this.el.nativeElement.style.backgroundPosition = position;
  }

  constructor(el: ElementRef) {
    this.el = el;
    this.el.nativeElement.style.backgroundPosition = this.backgroundPosition;
    this.mouseEventCount = 0;
  }

  @HostListener('mousemove') onMouseMove() {

    if(this.mouseEventCount < 5) {
     this.mouseEventCount ++;
     return;
     }

     this.mouseEventCount =0;

    if(event)
      this.mouseEvent = event;

    var backgroundPositionX = 50 - Math.round(((this.mouseEvent.layerX * 30) /  this.mouseEvent.target.clientWidth) - 15);
    var backgroundPositionY = 50 + Math.round(((this.mouseEvent.layerY * 30) /  this.mouseEvent.target.clientHeight) - 15);
    this.moveBackground(backgroundPositionX+'% '+backgroundPositionY+'%');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.moveBackground(this.backgroundPosition);
  }


}
