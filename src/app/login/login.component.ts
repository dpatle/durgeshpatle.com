/**
 * Created by durgesh on 4/3/17.
 */
import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './../shared/authentication.service';
import {ApplicationDataService} from '../shared/application-data.service';
import {WindowRefService} from '../shared/window-ref.service';

const pathCommand = 'guest@durgeshpatle.com: ~/login$';
const loginCommand = 'sudo apt-get login durgesh';
const passwordCommand = '[sudo] password for durgesh:';
const passwordError = 'Sorry, try again.';

@Component({
  selector: 'login-page',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  providers: [AuthenticationService, WindowRefService, ApplicationDataService]
})

export class LoginComponent implements OnInit {

  commands: string[] = [];
  currentCommand: string = '';
  pathCommand: string = pathCommand;
  isPasswordEntryInProgress: boolean = false;
  isAuthInProgress: boolean = false;

  constructor(private authenticationService: AuthenticationService,
              private windowRefService: WindowRefService,
              private applicationDataService: ApplicationDataService) {
  }

  commandEntered(event: any): void {
    var authPromise: any;
    if (event.charCode === 13) {
      if (this.isPasswordEntryInProgress) {
        this.commands.push(passwordCommand);
        authPromise = this.authenticationService.doLogin(this.currentCommand);
        this.pathCommand = '';
        this.currentCommand = '';
        this.isPasswordEntryInProgress = false;
        this.isAuthInProgress = true;
        authPromise.then((response) => {
          this.applicationDataService.setSelectedProfile('durgesh');
          this.navigateToUrl('/');
        }, (error) => {
          this.commands.push(passwordError);
          this.pathCommand = passwordCommand;
          this.currentCommand = '';
          this.isPasswordEntryInProgress = true;
        });
      }
    } else {

    }
  }

  askForCredential(): void {
    this.commands.push(pathCommand + ' ' + loginCommand);
    this.pathCommand = passwordCommand;
    this.currentCommand = '';
    this.isPasswordEntryInProgress = true;
  }

  navigateToUrl(url: string): void {
    this.windowRefService.getNativeWindow().location.href = url;
  }

  goBackToLastPage(): void {
    this.navigateToUrl('/');
  }

  ngOnInit(): void {
    this.askForCredential();
  }
}
